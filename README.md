1.	gather_examples.py – this script grabs face ROIs from input video files and helps us to create a deep learning face liveness dataset.
python gather_examples.py --input videos/fake.mp4 --output dataset/fake \
--detector face_detector --skip 1

2.	face_detector folder – open CV deep learning face detector 
3.	livenessnet.py –  deep learning-based liveness detector (simple CNN). Will be called by train.py.
Parameters:
•	width : How wide the image/volume is.
•	height : How tall the image is.
•	depth : The number of channels for the image (in this case 3 since we’ll be working with RGB images).
•	classes : The number of classes. We have two total classes: “real” and “fake”
•	model : model = Sequential()
•	inputShape: inputShape = (height, width, depth)
 
4.	train.py : As the filename indicates, this script will train our LivenessNet classifier. We’ll use Keras and TensorFlow to train the model. 

python train.py --dataset dataset --model liveness.model --le le.pickle

Input args:
•	--dataset : The path to the input dataset. Prior should be created the dataset with the gather_examples.py script containing fake and real faces.
•	--model : Our script will generate an output model file — here you supply the path to it.
•	--le : The path to our output serialized label encoder file also needs to be supplied.
•	--plot : The training script will generate a plot. If you wish to override the default value of "plot.png" , you should specify this value on the command line.
Output files:
•	le.pickle : Our class label encoder.
•	liveness.model : Our serialized Keras model which detects face liveness.
•	plot.png : The training history plot shows accuracy and loss curves so we can assess our model (i.e. over/underfitting).


5.	liveness_demo.py : Our demonstration script will fire up your webcam to grab frames to conduct face liveness detection in real-time.

